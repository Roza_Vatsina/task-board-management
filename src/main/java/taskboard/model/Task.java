package taskboard.model;

import javax.persistence.*;

@Entity
@Table(name = "tasks")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "list_id", nullable = false)
    private int listId;

    @Column(name = "parent_id", nullable = false)
    private int parentId;

    @Column(name = "position", nullable = false)
    private int position;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "due_to")
    private int dueTo;

    @Column(name = "done")
    private int done;

    @Column(name = "done_percent")
    private int donePercent;

    public Task(){
    }

    public Task(int id, int listId, int parentId, int position, String title, String description, int dueTo, int done, int donePercent) {
        this.id = id;
        this.listId = listId;
        this.parentId = parentId;
        this.position = position;
        this.title = title;
        this.description = description;
        this.dueTo = dueTo;
        this.done = done;
        this.donePercent = donePercent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDueTo() {
        return dueTo;
    }

    public void setDueTo(int dueTo) {
        this.dueTo = dueTo;
    }

    public int getDone() {
        return done;
    }

    public void setDone(int done) {
        this.done = done;
    }

    public int getDonePercent() {
        return donePercent;
    }

    public void setDonePercent(int donePercent) {
        this.donePercent = donePercent;
    }
}
