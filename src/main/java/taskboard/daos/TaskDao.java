package taskboard.daos;

import taskboard.model.Task;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

public class TaskDao {
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("PERSISTENCE");
	private EntityManager entityManager;

	public TaskDao() {
		entityManager = factory.createEntityManager();
	}

	@Transactional
	public Task addUpdateTask(Task task) {
		entityManager.getTransaction().begin();
		Task mergedTask = entityManager.merge(task);
		entityManager.flush();
		entityManager.getTransaction().commit();
		return mergedTask;
	}

	@Transactional
	public List<Task> moveTasksBetweenList(int oldListId, int newListId) {
		List<Task> tasks = getAllByListId(oldListId);
		List<Task> updatedTasks = new ArrayList<>();
		for (Task t : tasks) {
			t.setListId(newListId);
			addUpdateTask(t);
			updatedTasks.add(t);
		}
		return updatedTasks;
	}

	@Transactional
	public Task getById(int taskId) {
		return entityManager.find(Task.class, taskId);
	}

	@Transactional
	public List<Task> getAll() {
		return (List<Task>) entityManager.createNativeQuery(
				"SELECT * FROM Tasks", Task.class)
				.getResultList();
	}

	@Transactional
	public List<Task> getAllByListId(int listId) {
		return (List<Task>) entityManager.createNativeQuery(
				"SELECT * FROM Tasks WHERE list_id=:listId", Task.class)
				.setParameter("listId", listId)
				.getResultList();
	}

	@Transactional
	public void delete(int taskId) {
		Task task = entityManager.getReference(Task.class, taskId);
		entityManager.getTransaction().begin();
		entityManager.remove(task);
		entityManager.getTransaction().commit();
	}

	@Transactional
	public void deleteByListId(int listId) {
		entityManager.getTransaction().begin();
		entityManager.createNativeQuery(
				"DELETE * FROM Tasks WHERE list_id=:listId", Task.class)
				.setParameter("listId", listId);
		entityManager.getTransaction().commit();
	}
}