package taskboard.daos;

import taskboard.model.Task;
import taskboard.model.TaskList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

public class TaskListDao {
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("PERSISTENCE");
	private EntityManager entityManager;

	public TaskListDao() {
		entityManager = factory.createEntityManager();
	}

	@Transactional
	public TaskList addUpdateTaskList(TaskList tasklist) {
		entityManager.getTransaction().begin();
		TaskList mergedTaskList = entityManager.merge(tasklist);
		entityManager.flush();
		entityManager.getTransaction().commit();
		return mergedTaskList;
	}

	@Transactional
	public TaskList getById(int taskListId) {
		return entityManager.find(TaskList.class, taskListId);
	}

	@Transactional
	public List<TaskList> getAll() {
		return (List<TaskList>) entityManager.createNativeQuery(
				"SELECT * FROM lists", TaskList.class)
				.getResultList();
	}

	@Transactional
	public void delete(int listId) {
		TaskList tl = entityManager.getReference(TaskList.class, listId);
		entityManager.getTransaction().begin();
		entityManager.remove(tl);
		entityManager.getTransaction().commit();
	}


}