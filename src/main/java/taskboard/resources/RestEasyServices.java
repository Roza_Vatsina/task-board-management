package taskboard.resources;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/taskboard")
public class RestEasyServices extends Application {

    private Set < Object > singletons = new HashSet < Object > ();

    public RestEasyServices() {
        singletons.add(new TaskListResource());
        singletons.add(new TaskResource());
    }

    @Override
    public Set < Object > getSingletons() {
        return singletons;
    }
}