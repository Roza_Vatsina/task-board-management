package taskboard.resources;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import taskboard.daos.TaskListDao;
import taskboard.model.TaskList;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/tasklist")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TaskListResource {
    private static final Logger logger = LoggerFactory.logger(TaskResource.class);
    TaskListDao taskListDao = new TaskListDao();

    @POST
    @Path("/")
    @Transactional
    public TaskList createUpdateTaskList(TaskList taskList) {
        try {
            return taskListDao.addUpdateTaskList(taskList);
        } catch (Exception e) {
            Response response = Response.status(Response.Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON)
                    .entity(taskList).build();
            throw new BadRequestException("error", response);
        }
    }

    @GET
    @Path("/{id}")
    public TaskList getTaskList(@PathParam("id") int id) {
        try {
            TaskList tl = taskListDao.getById(id);
            return tl;
        } catch (Exception e) {
            logger.warn("error trying to get task list with id: " + id);
            throw new BadRequestException();
        }
    }

    @GET
    @Path("/all")
    public List<TaskList> getAllTasks() {
        try {
            return taskListDao.getAll();
        } catch (Exception e) {
            logger.warn("error trying to get all task lists");
            throw new BadRequestException();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response.Status deleteTaskList(@PathParam("id") int id) {
        try {
            taskListDao.delete(id);
            return Response.Status.OK;
        } catch (Exception e) {
            logger.warn("error trying to delete task list with id: " + id);
            throw new BadRequestException();
        }
    }

}
