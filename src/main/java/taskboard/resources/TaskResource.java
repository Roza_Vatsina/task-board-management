package taskboard.resources;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.hibernate.service.spi.InjectService;
import org.jboss.logging.Logger;
import taskboard.daos.TaskDao;
import taskboard.model.Task;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/task")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TaskResource {
    private static final Logger logger = LoggerFactory.logger(TaskResource.class);
    TaskDao taskDao = new TaskDao();

    @POST
    @Path("/")
    @Transactional
    public Task createUpdateTask(Task task) {
        try {
            return taskDao.addUpdateTask(task);
        } catch (Exception e) {
            Response response = Response.status(Response.Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON)
                    .entity(task).build();
            throw new BadRequestException("error", response);
        }
    }

    @POST
    @Path("/list/{old_id}/{new_id}")
    @Transactional
    public List<Task> moveTasksBetweenLists(@PathParam("old_id") int oldListId, @PathParam("new_id") int newListId) {
        try {
            return taskDao.moveTasksBetweenList(oldListId, newListId);
        } catch (Exception e) {
            logger.warn("error trying to move tasks from list with id: " + oldListId);
            throw new BadRequestException();
        }
    }

    @GET
    @Path("/{id}")
    public Task getTask(@PathParam("id") int id) {
        try {
            Task task = taskDao.getById(id);
            return task;
        } catch (Exception e) {
            logger.warn("error trying to get task with id: " + id);
            throw new BadRequestException();
        }
    }

    @GET
    @Path("/all")
    public List<Task> getAllTasks() {
        try {
            return taskDao.getAll();
        } catch (Exception e) {
            logger.warn("error trying to get all tasks");
            throw new BadRequestException();
        }
    }

    @GET
    @Path("/list/{id}")
    public List<Task> getTasksByListId(@PathParam("id") int id) {
        try {
            return taskDao.getAllByListId(id);
        } catch (Exception e) {
            logger.warn("error trying to get tasks in list with id: " + id);
            throw new BadRequestException();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response.Status deleteTask(@PathParam("id") int id) {
        try {
            taskDao.delete(id);
            return Response.Status.OK;
        } catch (Exception e) {
            logger.warn("error trying to delete task with id: " + id);
            throw new BadRequestException();
        }
    }

    @DELETE
    @Path("/list/{id}")
    public Response.Status deleteTasksByListId(@PathParam("id") int id) {
        try {
            taskDao.deleteByListId(id);
            return Response.Status.OK;
        } catch (Exception e) {
            logger.warn("error trying to delete tasks in list with id: " + id);
            throw new BadRequestException();
        }
    }
}
