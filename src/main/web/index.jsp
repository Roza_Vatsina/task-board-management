<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Task Board Manager</title>

    <%--    Stylesheets--%>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="/resources/css/taskboard.css" rel="stylesheet">

    <%--    Scripts--%>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/resources/js/taskboard.js"></script>
  </head>
  <body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <a class="nav-link" href="/">Task Board Manager</a>
    </nav>
    <div class="task-board"></div>

    <div id="edit-task-dialog" style="display: none" title="Edit Task">
      <form id="edit-task-form" >
        <fieldset>
          <label for="edit-task-title">Title</label>
          <input type="text" name="edit-task-title" id="edit-task-title" class="text ui-widget-content ui-corner-all">
          <label for="edit-task-description">Description</label>
          <div type="text" name="edit-task-description" id="edit-task-description" class="text ui-widget-content ui-corner-all"></div>
        </fieldset>
        <fieldset style="float: left; width: 27%;">
          <label for="edit-task-due-date">Due Date</label>
          <input type="text" name="edit-task-due-date" id="edit-task-due-date" class="text ui-widget-content ui-corner-all">
          <input type="hidden" name="edit-task-due-date-epoch" id="edit-task-due-date-epoch" class="text ui-widget-content ui-corner-all">
        </fieldset>

        <div id="users-invite-list" class="item-list"></div>


        <fieldset style="clear: left; padding-top: 20px">
          <label for="add-todo">Tasks</label>
          <div class="checklist-progress">
            <span class="checklist-progress-percentage">0%</span>
            <div class="checklist-progress-bar">
              <div class="checklist-progress-bar-current" style="width: 0%;"></div>
            </div>
          </div>
          <input type="text" name="add-todo" id="add-todo" class="text ui-widget-content ui-corner-all"
                 placeholder="Add Task...">
        </fieldset>

        <ul id="todo-list"></ul>

        <input type="hidden" id="edit-task-id">
        <input type="hidden" id="edit-task-list">
      </form>
    </div>

    <div id="dialog-delete-confirm" style="display: none" title="Delete the task?">
      <p><i class="fa fa-exclamation-triangle delete-confirmation-message"></i>
        The task will be permanently deleted and cannot be recovered. Are you sure?
      </p>
    </div>
  </body>
</html>
