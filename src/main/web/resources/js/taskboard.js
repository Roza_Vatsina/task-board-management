jQuery(document).ready(function(){
    let baseUrl = '/taskboard'

    // Enter Key detect
    jQuery.fn.enterKey = function (fnc) {
        return this.each(function () {
            jQuery(this).keypress(function (ev) {
                const keycode = (ev.keyCode ? ev.keyCode : ev.which);
                if (keycode === 13) {
                    fnc.call(this, ev);
                }
            })
        })
    };

    let isRefreshing = false;
    let isListInlineEditing = false;

    jQuery(function(){
        // Get available task lists
       
        
        //

        function renderTasks(id){
            jQuery.ajax({
                type: "GET",
                url: baseUrl + '/task/list/' + id,
                success: function(response){
                    const tasks = response;
                    let html = "";

                    if (tasks){
                        tasks.filter(task => task !== null).forEach(task => {
                            let dueEpoch = '';
                            let due = '';
                            if(task.dueTo !== null){
                                dueEpoch = task.dueTo;
                                due = jQuery.datepicker.formatDate('d M y',new Date(dueEpoch * 1000));
                            } else {
                                due = 'Due Date';
                            }

                            html += `<li class="task">
                                   <div class="task-progress">
                                        <div class="task-progress-bar">
                                            <div class="color-line task-progress-bar-current" style="width: `+task.donePercent+`%;"></div>
                                        </div>
                                    </div>
                                    <div class = 'task-wrapper' id="` + task.id + `">
                                        <div class="task-title-block">
                                            <div class="task-title">` + task.title + `</div>
                                            <div class="task-menu dropdown">
                                                <button type="submit" class="task-menu-button dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                                                <ul class="dropdown-menu drop-left">
                                                  <li><a href="#" class="edit-task-button editable"><i class="fa fa-pencil"></i>Edit</a></li>
                                                  <li class="divider"></li>
                                                  <li><a href="#" class="delete-task-button"><i class="fa fa-times"></i>Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="task-description editable">` + task.description + `</div>
                                        <div class="task-info-block">
                                            <div class="task-date">` + due + `</div>
                                            <input type="hidden" class="task-date-epoch" name="task-date-epoch" value="`+dueEpoch+`"/>
                                        </div>
                                    </div>
                                </li>`
                        })
                    }
                },
                error: function(response){
                    console.log('There was a problem fetching the tasks...')
                }
            })
        }

        function renderBoard(tasklists){
            if(isListInlineEditing){
                return;
            }

            let html = "";
            tasklists.forEach(tasklist => {
                html += `<div class="tasks-list-wrapper">
                            <div class="tasks-list" id="` + tasklist.id + `">
                                <div class="color-line-big"></div>
                                <h2 class="tasks-list-heading">
                                    <input type="text" class="tasks-list-heading" value="` + tasklist.name + `"/>
                                    <button type="submit" class="add-new-task-button"><i class="fa fa-plus"></i></button>
                                </h2>
                                <div class="tasks-list-body">
                                    <ul class="tasks">` + renderTasks(tasklist.id) + `<ul>
                                </div>
                            </div>
                        </div>`;
            });
            jQuery('.task-board').html(html);
            console.log('GTP');

            jQuery('.tasks-list > h2').on('click', function(e){
                console.log('lwer');
                const isInlineEditList = jQuery(e.target).is('input');
                const isAddTaskButton = jQuery(e.target).is('button');
                if(isInlineEditList){
                    const list = jQuery(this).closest('.tasks-list');
                    inlineEditList(list);
                } else if(isAddTaskButton){
                    console.log('lolo');

                    jQuery(this).addClass('autocomplete-loading');
                    isRefreshing = true;
                    const list_id = jQuery(e.target).closest('.tasks-list').attr('id');

                    const position = jQuery('#' + list_id).find('.task').length;

                    const newTask = {
                        'listId': list_id,
                        'parent_id': null,
                        'position': position,
                        'title': 'Click to edit the task...',
                        'description': 'Click to edit the task\'s description',
                        'dueTo': null,
                        'done': 0,
                        'donePercent': 0
                    };
                    jQuery.ajax({
                        type: "POST",
                        url: baseUrl + '/task',
                        data: newTask,
                        success: function(response){
                            console.log(response);
                        },
                        error: function(){
                            console.log('There was a problem fetching the taskboard...');
                        }
                    });
                    e.preventDefault();
                }
            });

        }

        function inlineEditList(taskList){
            isListInlineEditing = true;
            list.addClass('editing');

            const id = taskList.attr('id');
            const label = taskList.find('h2');
            const inline = jQuery('<input type="text" class="input-list" id="input-list' + id + '"/>');
            const oldTitle = label.text().trim();

            inline.val(oldTitle);
            label.html(inline);
            inline.focus();
            inline.enterKey(function(){inline.trigger('enterEvent');});

            inline.on('blur enterEvent',function(){
                const newTitle = jQuery('#input-list'+id+'').val();
                const newTitleLength = newTitle.length;

                label.append(newTitle + '<button type="submit" class="add-new-task-button"><i class="fa fa-plus"></i></button>');
                inline.remove();
                taskList.removeClass('editing');

                if (newTitleLength > 0 && newTitle !== oldTitle) {
                    isRefreshing = true;

                    label.addClass('autocomplete-loading');
                    const data = {
                        'id' : id,
                        'name':  newTitle
                    };

                    jQuery.ajax({
                        type: "POST",
                        url: baseUrl + '/tasklist',
                        data: data,
                        success: function(data){
                            label.removeClass('autocomplete-loading');
                            isRefreshing = false;
                            isListInlineEditing = false;
                        },
                        error: function(data){
                            label.removeClass('autocomplete-loading');
                            isRefreshing = false;
                            isListInlineEditing = false;
                        }
                    });
                }
            });
        }

    });

});



